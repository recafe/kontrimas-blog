<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }

    public function mostPopularArticles() {

        $articles = Article::orderBy('times_readed', 'desc')
            ->take(3)
            ->get();
        return $articles;
    }
    public function comment()
    {
        return $this->hasMany('App\Comments');
    }
}
