<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function article()
    {
        return $this->hasMany('App\Article');
    }

    public function getAllCategories()
    {
        $categories = Category::all();
        return $categories;
    }


}

