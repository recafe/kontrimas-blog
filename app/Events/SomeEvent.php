<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SomeEvent extends Event
{

    public $first;
    public $second;
    public $third;
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($first, $second, $third) {
        $this->first = $first;
        $this->second = $second;
        $this->third = $third;
    }

    public function getData()
    {
        return [
            'first' => $this->first,
            'second' => $this->second,
            'third' => $this->third,
        ];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
