<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleFormRequest;
use App\Category;
use App\Article;
use Auth;
use Illuminate\Support\Facades\Input;
use Image;
use App\Tag;
use Event;
use App\Events\ArticleReaded;




class ArticlesController extends Controller
{


    public function index()
    {
        $articles = Article::where('active',1)->orderBy('created_at','desc')->paginate(5);
        $title = 'Naujienos';
        return view('home')->withArticles($articles)->withTitle($title);
    }

    public function listArticles() {
        $articles = Article::orderBy('created_at','desc')->paginate(10);
        return view('articles.showList')->withArticles($articles);

    }

    public function editArticle ($id) {
        $article = Article::where('id',$id)->first();
        $categories = Category::lists('name', 'id')->toArray();
        $tags = Tag::where('article_id',$id)->get(); // distinct() nerodo reikiamo rezultato
        return view('articles.edit')->withArticle($article)->with('categories', $categories)->with('tags', $tags);
    }


    public function newArticle(Request $request)
    {
        if (Auth::check()) {
            $categories = Category::lists('name', 'id')->toArray();
            return view('articles.new')->with('categories', $categories);
        } else {
            return redirect('/login')->withErrors('Prašome prisijungti');
        }
    }


    public function getArticle($id)
    {
        $article = Article::where('id',$id)->first();
        if($article->active == 0) {
            return redirect('/')->withErrors('Puslapis nerastas');
        }

        \Event::fire(new ArticleReaded($article));
        return view('articles.show')->withArticle($article);
    }

    public function updateArticle(Request $request)
    {
        $article_id = $request->input('article_id');
        $article = Article::find($article_id);
        if($article && ($article->author_id == $request->user()->id))
        {
            $article->title = $request->input('title');
            $article->body = $request->input('body');
            $article->category_id = $request->get('category');
            $article->active = $request->get('active');

            if (Input::hasFile('articleImage')) {
                $image = Input::file('articleImage');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('pics/' . $filename);
                Image::make($image->getRealPath())->resize(900, 300)->save($path);
                $article->image = $filename;
            } else {
                $image= Category::find($request->get('category'))->image;
                $article->image = $image;
            }

            $tagDelete = Tag::where('article_id',$article->id);
            $tagDelete->delete();


            $tagsArray = explode(',',  $request->get('tags'));
            foreach($tagsArray as $tagContent){
                $tag = new Tag();
                $tag->article_id = $article->id;
                $tag->tag = $tagContent;
                $tag->save();

            }


            $message = "Straipsnis atnaujintas";
            $article->save();
            return redirect('/redaguoti-straipsni/'. $article_id)->withMessage($message);
        }
        else
        {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }


    public function search(Request $request) {
        $keyword = $request->get('keyword');
        $articles = Article::where('title', 'LIKE', '%' . $keyword . '%')->paginate(10);
        return view('search.show', compact('articles', 'keyword'));
    }


    public function saveArticle(Request $request)
    {

        $article = new Article();
        $article->title = $request->get('title');
        $article->body = $request->get('body');
        $article->author_id = $request->user()->id;
        $article->category_id = $request->get('category');
        $article->active = $request->get('active');

        if (Input::hasFile('articleImage')) {
            $image = Input::file('articleImage');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('pics/' . $filename);
            Image::make($image->getRealPath())->resize(900, 300)->save($path);
            $article->image = $filename;
        } else {
            $image= Category::find($request->get('category'))->image;
            $article->image = $image;

        }

            $message = 'Straipsnis išsaugotas sėkmingai.';

        $article->save();

        $tagsArray = explode(',',  $request->get('tags'));
        foreach($tagsArray as $tagContent){
            $tag = new Tag();
            $tag->article_id = $article->id;
            $tag->tag = $tagContent;
            $tag->save();

        }

        return redirect('/')->withMessage($message);
    }

    public function deleteArticle ($id) {
        $article = Article::find($id);
        $article->delete();
        $message = 'Straipsnis sėkmingai ištrintas';
        return redirect('/sukurti-straipsniai')->withMessage($message);


    }

}
