<?php

namespace App\Http\Controllers;

use Illuminate\Console\Scheduling\CallbackEvent;
use Illuminate\Http\Request;

use App\Article;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Image;
use Auth;

class CategoriesController extends Controller
{
    public function getArticlesByCategory($id)
    {
        $articles = Article::where('category_id',$id)->orderBy('created_at','desc')->paginate(5);
        $title = Category::find($id)->name;
        return view('categories.show')->withArticles($articles)->withTitle($title);
    }

    public function listCategories() {
        $categories = Category::orderBy('created_at','desc')->paginate(10);
        return view('categories.showList')->withCategories($categories);

    }

    public function newCategory()
    {
        if (Auth::check()) {
            return view('categories.create');
        } else {
            return redirect('/login')->withErrors('Prašome prisijungti');
        }
    }

    public function editCategory ($id) {
        $category = Category::where('id',$id)->first();
        return view('categories.edit')->withCategory($category);
    }


    public function saveCategory(Request $request)
    {

        $category = new Category();
        $category->name = $request->get('name');


        if (Input::hasFile('categoryImage')) {
            $image = Input::file('categoryImage');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('pics/' . $filename);
            Image::make($image->getRealPath())->resize(900, 300)->save($path);
            $category->image = $filename;
        }

            $message = 'Straipsnis išsaugotas sėkmingai.';
        $category->save();
        return redirect('/sukurtos-kategorijos')->withMessage($message);
    }


    public function updateCategory(Request $request)
    {
        $category_id = $request->input('category_id');
        $category = Category::find($category_id);
            $category->name = $request->input('name');


            if (Input::hasFile('categoryImage')) {
                $image = Input::file('categoryImage');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('pics/' . $filename);
                Image::make($image->getRealPath())->resize(900, 300)->save($path);
                $category->image = $filename;
            }

            $message = "Kategorija atnaujinta";
            $category->save();
            return redirect('/redaguoti-kategorija/'. $category_id)->withMessage($message);

    }


    public function deleteCategory ($id) {
        $category = Category::find($id);
        $category->delete();
        $message = 'Kategorija sėkmingai ištrinta';
        return redirect('/sukurtos-kategorijos')->withMessage($message);


    }

}
