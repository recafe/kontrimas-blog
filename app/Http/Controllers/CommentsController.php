<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;

class CommentsController extends Controller
{
    public function saveComment(Request $request)
    {
        $comment = new Comment();
        $article_id = $request->input('article_id');
        $comment->article_id = $article_id;
        $comment->body = $request->get('body');
        $comment->save();
        return redirect('/straipsniai/'. $article_id);
    }
}
