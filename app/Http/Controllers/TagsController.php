<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use DB;

class TagsController extends Controller
{


        public function findbyTag($tag) {

            $tags = Tag::select('articles.id', 'articles.title', 'articles.created_at', 'articles.body', 'articles.category_id', 'categories.name', 'articles.image')
                ->join('articles','tags.article_id','=','articles.id')
                ->join('categories','articles.category_id','=','categories.id')
                ->where('tags.tag','=',$tag)->get();
            return view('tags.show')->with('tags', $tags)->with('tag', $tag);


        }

}
