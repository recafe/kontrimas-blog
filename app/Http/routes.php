<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
View::composer('*', function($view)
{
    $view->with('categoriesAll',app('App\Category'));
});

View::composer('*', function($view)
{
    $view->with('listTags',app('App\Tag'));
});

View::composer('*', function($view)
{
    $view->with('popularArticles',app('App\Article'));
});

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'ArticlesController@index');
    Route::get('/naujas-straipsnis','ArticlesController@newArticle');
    Route::get('/kategorija/{id}','CategoriesController@getArticlesByCategory')->where('id', '[0-9]+');
    Route::post('/naujas-straipsnis','ArticlesController@saveArticle');

    Route::get('/nauja-kategorija','CategoriesController@newCategory');
    Route::post('/nauja-kategorija','CategoriesController@saveCategory');

    Route::get('/sukurti-straipsniai','ArticlesController@listArticles');
    Route::get('/sukurtos-kategorijos','CategoriesController@listCategories');

    Route::get('/redaguoti-kategorija/{id}','CategoriesController@editCategory')->where('id', '[0-9]+');
    Route::get('/redaguoti-straipsni/{id}','ArticlesController@editArticle')->where('id', '[0-9]+');

    Route::post('/redaguoti-straipsni','ArticlesController@updateArticle');
    Route::post('/redaguoti-kategorija','CategoriesController@updateCategory');

    Route::get('/istrinti-straipsni/{id}','ArticlesController@deleteArticle')->where('id', '[0-9]+');
    Route::get('/istrinti-kategorija/{id}','CategoriesController@deleteCategory')->where('id', '[0-9]+');


    Route::get('/straipsniai/{id}','ArticlesController@getArticle')->where('id', '[0-9]+');
    Route::get('/zymes/{tag}',['as' => 'article', 'uses' => 'TagsController@findByTag'])->where('tag', '[A-Za-z0-9-_]+');
    //Route::get('/paieska/{keyword}',['as' => 'search', 'uses' => 'ArticlesController@search'])->where('keyword', '[A-Za-z0-9-_]+');
    Route::post('/paieska','ArticlesController@search');

    Route::post('/naujas-komentaras','CommentsController@saveComment');




});


