<?php

namespace App\Listeners;

use App\Events\ArticleReaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleReadedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleReaded  $event
     * @return void
     */
    public function handle(ArticleReaded $event)
    {
        //dd($event);
        //dd($event->article->active);
        $event->article->increment('times_readed');
        $event->article->active += 1;
    }
}
