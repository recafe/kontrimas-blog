<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tag extends Model
{
    public function article()
    {
        return $this->hasMany('App\Article');
    }

    public function findMostPopularTags() {
        $tags = Tag::select('tag', DB::raw('count(tag) as count'))
            ->groupBy('tag')
            ->orderBy('count', 'desc')
            ->take(10)
            ->get();
        return $tags;
    }


}
