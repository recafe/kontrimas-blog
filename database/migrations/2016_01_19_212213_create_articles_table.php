<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function(Blueprint $table)
        {
            $table->increments('id');
            $table -> integer('author_id') -> unsigned() -> default(0);
			$table->foreign('author_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table -> integer('category_id') -> unsigned()
                    ->references('id')->on('categories')
                    ->onDelete('cascade');
			$table->string('title');
			$table->text('body');
            $table->integer('times_readed')->default(0);
            $table->string('image');
			$table->boolean('active');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
