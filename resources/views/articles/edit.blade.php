@extends('layouts.full')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>

    <link href="{{ asset('/css/jquery.tagit.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#post-body'
            });

            $('.btn-success').click(function(){
                $.ajax({
                    url: '/naujas-straipsnis',
                    type: "post",
                    data: $('#tags').tagit('assignedTags')
                });
            });

            $(function() {
                $('#tags').tagit({
                    singleField: true
                });
            });
        });
    </script>
    {!! Form::open(array('url'=>'/redaguoti-straipsni','method'=>'POST', 'files'=>true)) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="article_id" value="{{ $article->id }}{{ old('article_id') }}">
    <div class="form-group">
        <input required="required" value="@if(!old('title')){{$article->title}}@endif{{ old('title') }}" placeholder="Įveskite pavadinimą" type="text" name = "title"class="form-control" />
    </div>
    <div class="form-group">
        {!! Form::select('category', ($categories = array('' => 'Prašome pasirinkti kategoriją') + $categories), $article->category->id, ['class' => 'form-control', 'required' => "required"]) !!}
    </div>

    <div class="form-group">
        {!! Form::select('active', array('0' => 'Ne', '1' => 'Taip'), $article->active, ['placeholder' => 'Straipsnis viešas?', 'class' => 'form-control', 'required' => "required"]) !!}
    </div>
    @if(old('image'))
    <div class="form-group">
        <img class="img-responsive" src="/pics/{{$article->image}}" alt="">
    </div>
    @endif
    <div class="form-group">
        {!! Form::label('articleImage', 'Pasirinkitę straipsnio nuotrauką : ') !!}
        {!! Form::file('articleImage', ['class' => 'custom-file-input' ]) !!}
    </div>

    <div class="form-group">
        <label for="tags">Žymės (atskiriamos po tarpo)</label>
        <ul id="tags">
            @foreach( $tags as $tag )
                <li>{{$tag->tag}}</li>
            @endforeach
        </ul>
    </div>
    <div class="form-group">
        <textarea name='body' required="required"  id="post-body" class="form-control">
            @if(!old('body'))
                {!! $article->body !!}
            @endif
        </textarea>
    </div>
    <input type="submit" name='publish' class="btn btn-success" value = "Patvirtinti"/>
    {!! Form::close() !!}
@endsection
