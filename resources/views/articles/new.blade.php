@extends('layouts.full')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>
    <link href="{{ asset('/css/jquery.tagit.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function(){

            tinymce.init({
                selector: '#post-body'
            });


            $('.btn-success').click(function(){
                $.ajax({
                    url: '/naujas-straipsnis',
                    type: "post",
                    data: $('#tags').tagit('assignedTags')
                });
            });

            $(function() {
                $('#tags').tagit({
                    singleField: true
                });
            });
        });
    </script>

    {!! Form::open(array('url'=>'/naujas-straipsnis','method'=>'POST', 'files'=>true)) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label('name', 'Pasirinkite kategoriją ') !!}
        <input required="required" value="{{ old('title') }}" placeholder="Įveskite pavadinimą" type="text" name = "title"class="form-control" />
    </div>
        <div class="form-group">
            {!! Form::label('category', 'Pasirinkite kategoriją: ') !!}
            {!! Form::select('category',($categories = array('' => 'Prašome pasirinkti kategoriją') + $categories), null, array('class' => 'form-control', 'required' => "required")) !!}
        </div>
        <div class="form-group">
            {!! Form::label('active', 'Pasirinkite publikacijos tipą: ') !!}
            {!! Form::select('active', array('0' => 'Ne', '1' => 'Taip'), null, ['placeholder' => 'Straipsnis viešas?', 'class' => 'form-control', 'required' => "required"]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('articleImage', 'Pasirinkitę straipsnio nuotrauką (jei nepasirinksite, bus pritaikyta kategorijos nuotrauka) : ') !!}
            {!! Form::file('articleImage', ['class' => 'custom-file-input' ]) !!}
        </div>
        <div class="form-group">
            <label for="tags">Žymės (atskiriamos po tarpo)</label>
            <ul id="tags"></ul>
        </div>
    <div class="form-group">
        <textarea name='body' id="post-body" class="form-control">{{ old('body') }}</textarea>
    </div>
    <input type="submit" name='publish' class="btn btn-success" value = "Patvirtinti"/>
    {!! Form::close() !!}
@endsection
