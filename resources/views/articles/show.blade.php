<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>

@extends('layouts.app')
@section('content')
    <h1>{{$article->title}}</h1>
    <p class="lead">
        Kategorija: <a href="/kategorija/{{$article->category->id}}">{{ $article->category->name }}</a>
    </p>
    <hr>
    <p><span class="glyphicon glyphicon-time"></span>Publikuota: {{ $article->created_at->format('Y-m-d H:i') }}</p>
    <hr>
    <img class="img-responsive" src="/pics/{{$article->image}}" alt="">
    <hr>
        {!! $article->body !!}
    <div class="well">
        <h4>Palikite komentarą:</h4>
        {!! Form::open(array('url'=>'/naujas-komentaras','method'=>'POST')) !!}
        <input type="hidden" name="article_id" value="{{ $article->id }}{{ old('article_id') }}">

        <div class="form-group">
              <textarea name='body' required="required"  id="post-body" rows="3" class="form-control"></textarea>
            </div>
            <div class="g-recaptcha" data-sitekey="6Lft7BUTAAAAAApmqGaXKvDmbAEYFLYzTx0aKzXH"></div>

            <button type="submit" class="btn btn-primary">Skelbti</button>

        {!! Form::close() !!}
    </div>
    <hr>



@endsection
