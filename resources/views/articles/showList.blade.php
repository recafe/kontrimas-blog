<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
@extends('layouts.full')
@section('content')
    <div class="row">
        @foreach( $articles as $article )
    <div class="col-xs-12" style="padding-top: 20px;">
                <div class="col-xs-6">
                    <a href="/redaguoti-straipsni/{{$article->id}}"><h4>{{$article->title}}</h4></a>
                    Kategorija: <a href="/kategorija/{{$article->category}}">{{$article->category->name}}</a><br>
                    Publikuota: {{ $article->created_at->format('Y-m-d H:i') }}
                </div>
                <div class="col-xs-6" style="padding-top: 30px;">
                    <a href="/redaguoti-straipsni/{{$article->id}}" class="btn btn-primary" role="button">Redaguoti</a>
                    <a href="/istrinti-straipsni/{{$article->id}}" class="btn btn-danger" role="button">Ištrinti</a>
                </div>
        </div>
        @endforeach
    </div>
@endsection
