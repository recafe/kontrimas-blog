@extends('layouts.full')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    {!! Form::open(array('url'=>'/nauja-kategorija','method'=>'POST', 'files'=>true)) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <input required="required" value="{{ old('name') }}" placeholder="Įveskite pavadinimą" type="text" name = "name" class="form-control" />
    </div>
    <div class="form-group">
        {!! Form::label('categoryImage', 'Pasirinkitę kategorijos nuotrauką : ') !!}
        {!! Form::file('categoryImage', ['class' => 'custom-file-input', 'required' => "required" ]) !!}
    </div>
    <input type="submit" name='publish' class="btn btn-success" value = "Patvirtinti"/>
    {!! Form::close() !!}
@endsection
