@extends('layouts.full')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    {!! Form::open(array('url'=>'/redaguoti-kategorija','method'=>'POST', 'files'=>true)) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="category_id" value="{{ $category->id }}{{ old('category_id') }}">
    <div class="form-group">
        {!! Form::label('name', 'Kategorijos pavadinimas : ') !!}
        <input required="required" value="@if(!old('name')){{$category->name}}@endif{{ old('name') }}" placeholder="Įveskite pavadinimą" type="text" name = "name" class="form-control" />
    </div>
    @if(!old('image'))
        <div class="form-group">
            {!! Form::label('categoryImage', 'Dabartinė kategorijos nuoroda : ') !!}
            <img class="img-responsive" src="/pics/{{$category->image}}" alt="">
        </div>
    @endif
    <div class="form-group">
        {!! Form::label('categoryImage', 'Pasirinkitę naują kategorijos nuotrauką : ') !!}
        {!! Form::file('categoryImage', ['class' => 'custom-file-input' ]) !!}
    </div>
    <input type="submit" name='publish' class="btn btn-success" value = "Patvirtinti"/>
    {!! Form::close() !!}

@endsection
