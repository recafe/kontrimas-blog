<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
@extends('layouts.full')
@section('content')
    @foreach( $categories as $category )
  <div class="col-xs-12">
      <div class="row">
          <div class="col-xs-4 col-xs-offset-2">
              <a href="/redaguoti-kategorija/{{$category->id}}"><h4>{{$category->name}}</h4></a>
          </div>
          <div class="col-xs-6">
              <a href="/redaguoti-kategorija/{{$category->id}}" class="btn btn-primary" role="button">Redaguoti</a>
              <a href="/istrinti-kategorija/{{$category->id}}" class="btn btn-danger" role="button">Ištrinti</a>
          </div>
      </div>
  </div>
    @endforeach
@endsection
