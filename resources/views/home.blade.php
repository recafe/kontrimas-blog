@extends('layouts.app')
@section('content')
    <h1 class="page-header">
        <small> {{$title}}</small>
    </h1>
    @if ( !$articles->count() )
        Šiuo metu straipsnių nėra.
    @else
        <div>
            @foreach( $articles as $article )
                <div class="col-xs-12">

                <h2>
                    <a href="/straipsniai/{{$article->id}}">{{$article->title}}</a>
                </h2>
                <p>
                    Publikuota: {{ $article->created_at->format('Y-m-d H:i') }} kategorijoje: <a href="index.php">{{$article->category->name}}</a>
                </p>
                <hr>
                <img class="img-responsive" src="/pics/{{$article->image}}" alt="">
                <hr>
            <div>
                {!! str_limit($article->body, $limit = 1100, $end = '.......') !!}
            </div>
                <a class="btn btn-primary" style="float:right" href="/straipsniai/{{$article->id}}" >Skaityti daugiau</a>
                </div>
            @endforeach
        </div>
        @endif
    <hr>

@endsection
