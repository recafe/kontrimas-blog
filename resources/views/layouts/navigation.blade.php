<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Navigacija</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Pradžia</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Prisijungimas</a></li>
                <li><a href="{{ url('/register') }}">Registracija</a></li>
            @endif
            @if (Auth::user())
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Straipsniai<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/naujas-straipsnis') }}">Sukurti naują</a></li>
                        <li><a href="{{ url('/sukurti-straipsniai') }}">Redaguoti esamus</a></li>
                    </ul>
                </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Kategorijos<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/nauja-kategorija') }}">Sukurti naują</a></li>
                            <li><a href="{{ url('/sukurtos-kategorijos') }}">Redaguoti esamas kategorijas</a></li>
                        </ul>
                    </li>
                   {{-- <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Nustatymai<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/nustatymai-nuotraukos') }}">Nuotraukų</a></li>
                            <li><a href="{{ url('/nustatymai-puslapis') }}">Puslapio</a></li>
                        </ul>
                    </li>--}}
        </ul>
        <ul class="nav navbar-nav" style="float:right">
            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Atsijungti</a></li>
        </ul>
        @endif
    </div>
</div>