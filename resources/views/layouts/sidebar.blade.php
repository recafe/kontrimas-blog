<div class="well">
    <h3>Paieška</h3>
    <div class="input-group">
        <form action="/paieska" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="keyword"></label>
            <div class="form-group">
            <input type="text" class="form-control" id="keyword" name="keyword">
                </div>
            <input type="submit" class="btn btn-default"  value="Ieškoti" style="margin-top: 10px">
        </form>
    </div>
</div>

<div class="well col-xs-12">
    <h3>Skaitomiausi straipsniai</h3>
    @foreach( $popularArticles->mostPopularArticles() as $article )
        <div class="col-xs-12" style="padding-bottom: 15px">
            <a href="/straipsniai/{{$article->id}}"><h5>{{$article->title}}</h5></a>
        <img src = "/pics/{{$article->image}}" height="50px" width="150px">
        </div>
    @endforeach
</div>

<div class="well col-xs-12">
    <h3>Kategorijos</h3>
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-unstyled">

                @foreach( $categoriesAll->getAllCategories() as $category )
                    <li><a href="/kategorija/{{$category->id}}"><h4>{{$category->name}}</h4></a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="well col-xs-12">
    <h3>Populiariausios žymės</h3>
    @foreach( $listTags->findMostPopularTags() as $tag )
        <div class="col-xs-6"><a href="/zymes/{{$tag->tag}}"><h4>{{$tag->tag}}</h4></a></div>
    @endforeach
</div>