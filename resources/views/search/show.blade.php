@extends('layouts.app')
@section('content')
    <h1 class="page-header">
        <small>Raktinis žodis: {!! $keyword !!}</small>
    </h1>
    @if ( !$articles->count() )
        Pagal jūsu paieška nerasta jokių straipsnių.
    @else
        <div class="">
            @foreach( $articles as $article )
                <h2>
                    <a href="/straipsniai/{{$article->id}}">{{$article->title}}</a>
                </h2>
                <p>
                    Publikuota: {{ $article->created_at->format('Y-m-d H:i') }} kategorijoje: <a href="/kategorija/{{$article}}">{{$article->name}}</a>
                </p>
                <hr>
                <img class="img-responsive" src="/pics/{{$article->image}}" alt="">
                <hr>
                <div class="col-xs-12">
                    {!! str_limit($article->body, $limit = 1100, $end = '.......') !!}
                </div>
                <a class="btn btn-primary" style="float:right" href="/straipsniai/{{$article->id}}" >Skaityti daugiau</a>
            @endforeach
        </div>
    @endif

    <hr>


@endsection
